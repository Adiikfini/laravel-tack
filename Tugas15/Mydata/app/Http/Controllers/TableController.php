<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TableController extends Controller
{
    public function Table(){
        return view('table.data-table');
    }

    public function data(){
        return view('table.table');
    }
}
