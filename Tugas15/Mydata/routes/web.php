<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('Home','MasterController@Master');

Route::get('cast','CastController@index');
Route::get('cast/create','CastController@create');
Route::post('cast','CastController@store');
Route::get('cast/{cast_id}','CastController@show');
Route::get('cast/{cast_id}/edit','CastController@edit');
Route::put('cast/{cast_id}','CastController@update');
Route::delete('cast/{cast_id}','CastController@destroy');

Route::get('/', 'HomeController@Home');
Route::get('/Register', 'AuntController@form');
Route::post('/Welcome', 'AuntController@Welcome');
Route::get('/data-table', 'TableController@Table');
Route::get('/Table', 'TableController@data');
