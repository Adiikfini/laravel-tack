@extends('layout.master')

@section('judul')
    Cast input    
@endsection

@section('judul2')
    Masukan nama pemeran
@endsection

@section('isi')
  <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Quick Example</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
     <form action="/cast" method="POST">
            @csrf
            <div class="form-group">
                <label for="nama">Nama</label>
                <input type="text" class="form-control" name="nama" id="nama" placeholder="Masukkan Nama pemeran">
                @error('nama')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="umur">Usia</label>
                <input type="number" class="form-control" name="umur" id="umur" placeholder="Usia">
                @error('umur')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="bio">Biodata</label>
                <input type="text" class="form-control" name="bio" id="bio" placeholder="Deskipsi">
                @error('bio')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Tambah</button>
        </form>
            </div>
  
@endsection



