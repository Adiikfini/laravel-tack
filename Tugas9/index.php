<?php

require('animal.php');
require('ape.php');
require('Frog.php');

    $sheep = new Animal("shaun");

    echo "Name : $sheep->name <br>"; // "shaun"
    echo "legs : $sheep->legs <br>"; // 4
    echo "cold_blooded : $sheep->cold_blooded <b>"; // "no"
    
    echo "<br> <br>";

    $kodok = new Frog("buduk");
    echo "Name : $kodok->name <br>"; // "shaun"
    echo "legs : $kodok->legs <br>"; // 4
    echo "cold_blooded : $kodok->cold_blooded <br>"; // "no"
    echo $kodok->jump("jump"); // "hop hop"

    echo "<br> <br>";

    $sungokong = new Ape("kera sakti");
    echo "Name : $sungokong->name <br>"; // "shaun"
    echo "legs : $sungokong->legs <br>"; // 2
    echo "cold_blooded : $sungokong->cold_blooded <br>"; // "no"
    echo $sungokong->yell("Yell"); // "Auooo"


?>